const assert = require('assert');
const mongoose = require('mongoose');

const {tagModel} = require("./Tag");



describe("Start mongoose", async () =>
{
  it("Connection DB", async () =>
  {
    try
    {
      await connectDB();
      console.log("CONNECTED");
      await mongoose.connection.db.dropCollection('tags');

      for (let i = 0; i <  10; i++)
        await tagModel.create({label : "Test"});


    }
    catch (e)
    {
      console.error(e);
      exit(1);
    }
  });
});


describe('Find', function ()
{
  it ("FindAll & Count", async () =>
  {
    let a  = await tagModel.find({label: "Test"});
    let b = await tagModel.count({label: "Test"});

    assert.equal(a.length, b);
    assert.equal(b, 10);
  });

  it("Paginate", async () =>
  {
    let a = await tagModel.find({label: "Test"}).pagination(5, 10);
    assert.equal(a.length, 5);
  });
});



function connectDB()
{
  return new Promise(async (resolve, reject) =>
  {
    mongoose.connect("mongodb://localhost/testPlugin", {})
    .then(() =>
    {
      resolve();
    })
    .catch(err =>
    {
      reject(err);
    });
  });
}
