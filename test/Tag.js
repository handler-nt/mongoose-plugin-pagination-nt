const mongoose = require('mongoose');
const plugin = require('../index');

const tagSchema = new mongoose.Schema(
{
  label: {type: String, required: true}
},
{
  collection: "tags"
});

tagSchema.plugin(plugin.paginationManager);

const tagModel = mongoose.model('Tag', tagSchema);

module.exports = {tagModel};