function paginationManager(schema, options)
{
  /**
   * Pagination static
   * @param {Number} offset
   * @param {Number} limit
   * @return {*}
   */
  schema.query["pagination"] = function (offset, limit)
  {
    return this.skip(parseInt(offset)).limit(parseInt(limit));
  }
}

module.exports =
{
  paginationManager
};
