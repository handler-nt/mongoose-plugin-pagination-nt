# Mongoose Plugin Pagination


## How to use it 

* Import the plugin

	```javascript
	const paginationPlugin = require('mongoose-plugin-pagination-nt');

* Add the plugin to your Mongoose schema

	```javascript
	schema.plugin(paginationPlugin.paginationManager);

* In your query

<code>Model.find({label: "Test"}).pagination(0, 50);
	